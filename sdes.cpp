#include <iostream>
#include <vector>

using namespace std;

// 8 bit text input functions ========================================
void input_bits(vector<int> &bits);
void fill_bits(vector<int> &bits);

// key generation functions ==========================================
void p_10(vector<int> &key);
void split(const vector<int>& key, vector<int>& k_00, vector<int>& k_01);
void sl_1(vector<int>& key);
void merge(vector<int>& key, const vector<int>& k_00, const vector<int>& k_01);
void p10_w8(vector<int>&key);
void sl_2(vector<int>& key);
void round_2(vector<int>& key, vector<int>& k_00, vector<int>& k_01);
void gen_keys(vector<int>& key, vector<int>& key_2);

// encryption and decryption functions ===============================
void pw(vector<int>& bits);
void p4_w8(vector<int>& seq);
void p4(vector<int>& seq);
void po(vector<int>& seq);
void my_xor(vector<int>& seq, const vector<int>& key);
void sbox(vector<int>& part_a, vector<int>& part_b);
void encrypt(vector<int>& bits, vector<int>& key, vector<int>& key_2);
void decrypt(vector<int>& bits, vector<int>& key, vector<int>& key_2);
void cross(vector<int>& seq);

int main()
{
    //vector<int> bits {1, 0, 1, 1 ,1, 0, 0, 0};
    vector<int> bits;
    vector<int> key {1, 1, 0, 0, 0, 0, 0, 0, 1, 1};
    vector<int> key_1 = key;
    vector<int> key_2;

   input_bits(bits);
    encrypt(bits, key_1, key_2);
    key_1.clear();
    key_2.clear();
    key_1 = key;
    decrypt(bits, key_1, key_2);
    

}

// 8 bit text input functions ========================================
void input_bits(vector<int> &bits){
    cout << "Input bit sequence no longer than 8 if end write ';': " << endl;
    int x;
    while (cin >> x and bits.size() < 8)
        bits.push_back(x);
    fill_bits(bits);

    cout << "Original text:    ";
    for (int x : bits) cout << x << " ";
    cout << endl;
}
// fills bit sequence with 0s if it's length < 8;
void fill_bits(vector<int> &bits){
    if (bits.size() < 8){
        for (int i = bits.size() - 1; i < 7; i++)
            bits.push_back(0);
    }
}
//====================================================================

// key generation functions ==========================================

// scaffold for generating 1st and 2nd round keys
void gen_keys(vector<int>& key, vector<int>& key_2){
    vector<int> k_00;
    vector<int> k_01;

    p_10(key);
    split(key, k_00, k_01);
    sl_1(k_00);
    sl_1(k_01);
    merge(key, k_00, k_01);
    p10_w8(key);
    round_2(key_2, k_00, k_01);

    cout << "First round key:  ";
    for (int x : key) cout << x << " ";
    cout << endl;

    cout << "Second round key: ";
    for (int x : key_2) cout << x << " ";
    cout << endl;      
}
// p10 permutation
void p_10(vector<int> &key){
    vector<int> temp = key;

    key[0] = temp[2];
    key[1] = temp[4];
    key[2] = temp[1];
    key[3] = temp[6];
    key[4] = temp[3];
    key[5] = temp[9];
    key[6] = temp[0];
    key[7] = temp[8];
    key[8] = temp[7];
    key[9] = temp[5];
}
// splits key into to halves
void split(const vector<int>& key, vector<int>& k_00, vector<int>& k_01){
    for (int i = 0; i < key.size(); i++){
        if (i < key.size()/2) k_00.push_back(key[i]);
        else k_01.push_back(key[i]);
    }
}
// moving sequence by one position to the left
void sl_1(vector<int>& key){
    vector<int> temp = key;

    for (int i = 0; i < key.size(); i++){
        key[i] = temp[i + 1];
        if (i == key.size() -1) key[i] = temp[0];
    }
}
// merging two 5 bit keys int one 10 bit key
void merge(vector<int>& key, const vector<int>& k_00, const vector<int>& k_01){
    int x = key.size();
    key.clear();
    for (int i = 0; i < k_00.size(); i++){
        key.push_back(k_00[i]);
    }
    for (int i = 0; i < k_01.size(); i++){
        key.push_back(k_01[i]);
    }
}
// p10w8 operation + shrinking key by two bits
void p10_w8(vector<int>& key){
    vector<int> temp = key;
    key[0] = temp[5];
    key[1] = temp[2];
    key[2] = temp[6];
    key[3] = temp[3];
    key[4] = temp[7];
    key[5] = temp[4];
    key[6] = temp[9];
    key[7] = temp[8];
    
    key.pop_back();
    key.pop_back();
    

}
// scaffold for generating 2nd round key
void round_2(vector<int>& key, vector<int>& k_00, vector<int>& k_01){
    sl_2(k_00);
    sl_2(k_01);
    merge(key, k_00, k_01);
    p10_w8(key);
}
// sl2 operation
void sl_2(vector<int>& key){
    vector<int> temp = key;
    for (int i = 0; i < key.size(); i++){
        key[i] = temp[i+2];
        if (i == key.size() - 2) key[i] = temp[0];
        else if (i == key.size() - 1) key[i] = temp[1];
    }
}
//====================================================================

//encryption functions ===============================================

// 1st permutation
void pw(vector<int>& bits){
    vector<int> temp;
    temp = bits;

    bits[0] = temp[1];
    bits[1] = temp[5];
    bits[2] = temp[2];
    bits[3] = temp[0];
    bits[4] = temp[3];
    bits[5] = temp[7];
    bits[6] = temp[4];
    bits[7] = temp[6];
}


void p4_w8(vector<int>& seq){
    vector<int> temp;
    temp = seq;

    seq.clear();
    seq.push_back(temp[3]);
    seq.push_back(temp[0]);
    seq.push_back(temp[1]);
    seq.push_back(temp[2]);
    seq.push_back(temp[1]);
    seq.push_back(temp[2]);
    seq.push_back(temp[3]);
    seq.push_back(temp[0]);
}

// p4 operation
void p4(vector<int>& seq){
    vector<int> temp;
    temp = seq;

    seq[0] = temp[1];
    seq[1] = temp[3];
    seq[2] = temp[2];
    seq[3] = temp[0];
}

// reverse permutation
void po(vector<int>& seq){
    vector<int> temp;
    temp = seq;
    
    seq[0] = temp[3];
    seq[1] = temp[0];
    seq[2] = temp[2];
    seq[3] = temp[4];
    seq[4] = temp[6];
    seq[5] = temp[1];
    seq[6] = temp[7];
    seq[7] = temp[5];
}
//xor
void my_xor(vector<int>& seq, const vector<int>& key){
    for (int i = 0; i < seq.size(); i++){
        if (seq[i] == key[i]) seq[i] = 0;
        else if (seq[i] != key[i]) seq[i] = 1;
    }
}
//sbox functions
void sbox(vector<int>& part_a, vector<int>& part_b){
    int sbox_1[4][4]  {{1, 0, 3, 2},
                       {3, 2, 1, 0},
                       {0, 2, 1, 3},
                       {3, 1, 3, 2}};

    int sbox_2[4][4]  {{0, 1, 2, 3},
                       {2, 0, 1, 3},
                       {3, 0, 1, 0},
                       {2, 1, 0, 3}};

    int temp;

    // part_a and sbox_1 ===============================================
    if      (part_a[0] == 0 and part_a[3] == 0) part_a[0] = 0;
    else if (part_a[0] == 0 and part_a[3] == 1) part_a[0] = 1;
    else if (part_a[0] == 1 and part_a[3] == 0) part_a[0] = 2;
    else if (part_a[0] == 1 and part_a[3] == 1) part_a[0] = 3;

    if      (part_a[1] == 0 and part_a[2] == 0) part_a[1] = 0;
    else if (part_a[1] == 0 and part_a[2] == 1) part_a[1] = 1;
    else if (part_a[1] == 1 and part_a[2] == 0) part_a[1] = 2;
    else if (part_a[1] == 1 and part_a[2] == 1) part_a[1] = 3;

    part_a.pop_back();
    part_a.pop_back();

    temp = sbox_1[part_a[0]][part_a[1]];
    if (temp == 0){
        part_a[0] = 0;
        part_a[1] = 0;
    }
    else if (temp == 1){
        part_a[0] = 0;
        part_a[1] = 1;
    }
    else if (temp == 2){
        part_a[0] = 1;
        part_a[1] = 0;
    }
    else if (temp == 3){
        part_a[0] = 1;
        part_a[1] = 1;
    }
    //=================================================================        
    
    // part_b and sbox_2 ==============================================
    if      (part_b[0] == 0 and part_b[3] == 0) part_b[0] = 0;
    else if (part_b[0] == 0 and part_b[3] == 1) part_b[0] = 1;
    else if (part_b[0] == 1 and part_b[3] == 0) part_b[0] = 2;
    else if (part_b[0] == 1 and part_b[3] == 1) part_b[0] = 3;

    if      (part_b[1] == 0 and part_b[2] == 0) part_b[1] = 0;
    else if (part_b[1] == 0 and part_b[2] == 1) part_b[1] = 1;
    else if (part_b[1] == 1 and part_b[2] == 0) part_b[1] = 2;
    else if (part_b[1] == 1 and part_b[2] == 1) part_b[1] = 3;

    part_b.pop_back();
    part_b.pop_back();

    temp = sbox_2[part_b[0]][part_b[1]];
    if (temp == 0){
        part_b[0] = 0;
        part_b[1] = 0;
    }
    else if (temp == 1){
        part_b[0] = 0;
        part_b[1] = 1;
    }
    else if (temp == 2){
        part_b[0] = 1;
        part_b[1] = 0;
    }
    else if (temp == 3){
        part_b[0] = 1;
        part_b[1] = 1;
    }
}
// cross function
void cross(vector<int>& seq){
    vector<int> temp;
    temp = seq;
    for(int i = 0; i < 4; i++){
        seq[i] = temp[i + 4];
    }
    for(int i = 4; i < 8; i++){
        seq[i] = temp[i - 4];
    }
}
// splits in two
void split(const vector<int>& key, vector<int>& k_00, vector<int>& k_01);

void encrypt(vector<int>& bits, vector<int>& key, vector<int>& key_2){
    vector<int> part_1;
    vector<int> part_2;
    vector<int> part_2_copy;
    pw(bits);
    
    gen_keys(key, key_2);

    split(bits, part_1, part_2);
    part_2_copy = part_2;
    p4_w8(part_2_copy);
    my_xor(part_2_copy, key);

    vector<int> part_a;
    vector<int> part_b;

    split(part_2_copy, part_a, part_b);
    sbox(part_a, part_b);
    merge(part_2_copy, part_a, part_b);
    p4(part_2_copy);
    my_xor(part_1, part_2_copy);
    bits.clear();
    merge(bits, part_1, part_2);
    
    part_1.clear();
    part_2.clear();
    part_2_copy.clear();
    part_a.clear();
    part_b.clear();

    //=================================================================
    // cout << "After encrypting with 1st round key: ";
    // for (int x : bits) cout << x << " ";
    // cout << endl;
    cross(bits);
    // cout << "After crossing function: ";
    // for (int x : bits) cout << x << " ";
    // cout << endl;
    //================================================================
    split(bits, part_1, part_2);
    part_2_copy = part_2;
    p4_w8(part_2_copy);
    my_xor(part_2_copy, key_2);

    split(part_2_copy, part_a, part_b);
    sbox(part_a, part_b);
    merge(part_2_copy, part_a, part_b);
    p4(part_2_copy);
    my_xor(part_1, part_2_copy);
    bits.clear();
    merge(bits, part_1, part_2);

    // cout << "After encrypting with 2nd round key: ";
    // for (int x : bits) cout << x << " ";
    // cout << endl;

    po(bits);
    cout << "Encrypted text:   ";
    for (int x : bits) cout << x << " ";
    cout << endl;

    

}
void decrypt(vector<int>& bits, vector<int>& key, vector<int>& key_2){
    vector<int> part_1;
    vector<int> part_2;
    vector<int> part_2_copy;

    pw(bits);
    gen_keys(key, key_2);

    split(bits, part_1, part_2);
    part_2_copy = part_2;
    p4_w8(part_2_copy);
    my_xor(part_2_copy, key_2);

    vector<int> part_a;
    vector<int> part_b;

    split(part_2_copy, part_a, part_b);
    sbox(part_a, part_b);
    merge(part_2_copy, part_a, part_b);
    p4(part_2_copy);
    my_xor(part_1, part_2_copy);
    bits.clear();
    merge(bits, part_1, part_2);

    part_1.clear();
    part_2.clear();
    part_2_copy.clear();
    part_a.clear();
    part_b.clear();

    cross(bits);

    split(bits, part_1, part_2);
    part_2_copy = part_2;
    p4_w8(part_2_copy);
    my_xor(part_2_copy, key);

    split(part_2_copy, part_a, part_b);
    sbox(part_a, part_b);
    merge(part_2_copy, part_a, part_b);
    p4(part_2_copy);
    my_xor(part_1, part_2_copy);
    bits.clear();
    merge(bits, part_1, part_2);
    
    po(bits);
    cout << "Decrypted:        ";
    for (int x : bits) cout << x << " ";
    cout << endl;
}